const express = require('express');
const passport = require('passport');
const xsenv = require('@sap/xsenv');
const JWTStrategy = require('@sap/xssec').JWTStrategy;
const app = express();
const log = require('cf-nodejs-logging-support');

const services = xsenv.getServices({ uaa:'cfdemo'});

log.setLoggingLevel('info');

app.use(log.logNetwork);

app.get('/health', function (req, res) {
    req.logger.info("Got health check");
    res.sendStatus(200);
});

passport.use(new JWTStrategy(services.uaa));
app.use(passport.initialize());
app.use(passport.authenticate('JWT', {session: false}));

app.get('/', function (req, res) {
    const isAuthorized = req.authInfo.checkScope('$XSAPPNAME.Display');
    if (isAuthorized) {
        res.send('Hello ' + req.user.name.givenName + ', your ID is: ' + req.user.id + ' and your email is ' + req.user.emails[0].value + ' right?');
    } else {
        res.sendStatus(403);
    }
    });

app.get('/check', function (req, res) {
    res.send('Checked!');
});

app.get('/admin', function (req, res) {
    const isAuthorized = req.authInfo.checkScope('$XSAPPNAME.Admin');
    if (isAuthorized) {
        res.send('Hello ' + req.user.name.givenName + ' you are an admin!');
    } else {
        res.sendStatus(403);
    }

});

const port = process.env.PORT || 3000;
app.listen(port, function() {
    log.info('Server is listening on port %d', port);
});
